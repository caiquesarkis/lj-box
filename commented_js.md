---
title: "JS simulation explained step-by-step"
author: Julien Chopin adn Caique Sarkis
---



---

- [ ] Instead of position, I think it is better (at least easier to read) using X, Y, vX, vY, aX, and aY. Unless adding new variables would slow down the simulation...



---



In this article, I'll explain how the JS code written by [Caique Sarkis](https://gitlab.com/caiquesarkis) to simulate the interaction between two Lennard-Jones atoms.

A Lennard-Jones atom is a [famous model](https://en.wikipedia.org/wiki/Lennard-Jones_potential) developped by [John Lennard-Jones](https://en.wikipedia.org/wiki/John_Lennard-Jones) characterized by a potential of interaction of the form:
$$
V_{LJ}(r) = 4\epsilon \left(\left(\frac{\sigma}{r}\right)^{12} - \left(\frac{\sigma}{r}\right)^{6}\right)
\label{eq:VLJ}
$$
where $r$ is the distance between the two interacting atoms, $\epsilon$ is the depth of the potential well, $\sigma$ is related to the atom size. . 

> This model makes sense for noble gas like Helium or Argon, less so for the others. Yet, the model have the essential ingredients of atomic interaction, namely, hard-core repulsion and long range attraction. With this two features alone, many phenomena such as phase transitions, adhesion, etc.... can be reproduced.

In this script, we obtain the dynamics of atoms by integration of Newton's second law using the Verlet's algorithm. 

From Eq.$\ref{eq:VLJ}$, we obtain the force exerted by one atom on the other :
$$
{\bf F_{LJ}} = - \nabla\cdot V_{LJ} =\frac{24\epsilon}{r}\left(\left(\frac{\sigma}{r}\right)^{6} - 2\left(\frac{\sigma}{r}\right)^{12}\right) {\bf \hat{r}}
$$

> To accelerate the simulation, a truncated LJ potential is used. Atoms do not interact anymore when their distance is larger than $r_c$ . 

## Code explained

### Insert JS in a html page

The code is inserted in a html page `index.html` 

```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/Main.css">
	<title>LJ Box</title>

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet"> 

</head>
<body onload="startPause()"> 
	<canvas id = "thecanvas"></canvas>
	<script src="js/three.js"></script>
	<script src="js/Main.js"></script>
</body>
</html>
```

> What these two lines are for? Can we remove them?
>
> ```html
> <link rel="preconnect" href="https://fonts.gstatic.com">
> 	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet"> 
> ```

The script can be either linked to the html page by a URL or written between `<script>` elements. 

> I'm not sure to understand where to add the script. The location matters right?

### Run the script

In `body` element, you can see `onload="startPause()"`. This means that the function `startPause` is ran when the page loaded on your browser.

> does this has to be in the body tag? Can it be in the canvas tag?



### Display

The <canvas> element  ([Wikipedia](https://en.wikipedia.org/wiki/Canvas_element), [w3schools](https://www.w3schools.com/tags/tag_canvas.asp),[MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement)) is used to draw graphics, on the fly, via scripting  (usually JavaScript).

However, the <canvas> element has no drawing abilities of its own (it is only  a container for graphics) - you must use a script to actually draw the graphics.

The `getContext()` method returns an object that provides methods and  properties for drawing on the canvas.

This [reference](https://www.w3schools.com/graphics/canvas_reference.asp) will cover the properties and methods of the getContext("2d")  object, which can be used to draw text, lines, boxes, circles, and more - on the  canvas. 

We start by creating a `canvas` object using the *Document* method the `getElementById` and a `context` object  with the method `getContext`.  Information on the `getElementById` and `getContext` methods can be found [here](https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById) and [here](https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/getContext).

```javascript
let canvas = document.getElementById('thecanvas');
let context = canvas.getContext('2d');
```

**Size property of the canvas**

> in the css, I think we should give a size to the canvas element. At least width:100% and heigh:100%

```javascript
let width = window.innerWidth;
let height = window.innerHeight;
context.canvas.width  = width;
context.canvas.height = height;
```

The method `innerWidth` allows to get the width of the window which can vary depending on the user screen. See more info on this method [here](https://developer.mozilla.org/en-US/docs/Web/API/Window/innerWidth) and more general information on the `window` API [here](https://developer.mozilla.org/en-US/docs/Web/API/Window).

## Choice of rendering  methods

We are not using Three.js



### List of variables

#### Integration variable

| Variables | Definitions |
| --------- | ----------- |
| dt        |             |

 #### System geometry

| Variables | Definition |
| --------- | ---------- |
| radius    |            |
| boxWidth  |            |

#### Physical parameters

| Variables | Definition |
| --------- | ---------- |
| epsilon   |            |
| sigma     |            |

## functions

### drawCircles

```javascript
function drawCircles(i){
    context.beginPath();
    context.arc(position[i][0], position[i][1], radius/2, 0, 2*Math.PI);
    //context.fillStyle = '#000';
    context.fillStyle = 'hsl(0,80%,'+ distance(position) +'%)';    
    context.fill(); 
    context.closePath();
}
```

## Back to basics

### Html document

An HTML document is a plaintext document structured with elements. Elements are surrounded by matching opening and closing tags. Each tag begins and ends with angle brackets (`<>`). 

#### Elements

An **element** is a part of a webpage. In [XML](https://developer.mozilla.org/en-US/docs/Glossary/XML) and [HTML](https://developer.mozilla.org/en-US/docs/Glossary/HTML), an element may contain a data item or a chunk of text or an image, or  perhaps nothing. A typical element includes an opening tag with some [attributes](https://developer.mozilla.org/en-US/docs/Glossary/Attribute), enclosed text content, and a closing tag.

![anatomy-of-an-html-element](media/anatomy-of-an-html-element.png)

Elements and [tags](https://developer.mozilla.org/en-US/docs/Glossary/Tag) are *not* the same things. Tags begin or end an element in source code, whereas elements are part of the [DOM](https://developer.mozilla.org/en-US/docs/Glossary/DOM), the document model for displaying the page in the [browser](https://developer.mozilla.org/en-US/docs/Glossary/Browser).

