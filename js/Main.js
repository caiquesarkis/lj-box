let canvas = document.getElementById('thecanvas');
let context = canvas.getContext('2d');
let width = window.innerWidth;
let height = window.innerHeight;
context.canvas.width  = width;
context.canvas.height = height;


// Variables
let colors = [0x03071e]
let running = false;
let stepsPerFrame = 25;
let dt = 0.001;
let radius = 50;
if( /Android|webOS|iPhone|iPad|Mac|Macintosh|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    radius = 25;
   }

let initDist = 60//parseFloat(document.getElementById("initialD").value);
let oldPosition = [[(width/2)+initDist,height/2],[(width/2)-initDist,height/2]];
let position = [[(width/2)+initDist,height/2],[(width/2)-initDist,height/2]];
let aceleration = [[0,0],[0,0]];
let boxWidth = canvas.width ;
var wallStiffness = 50;	

// Initial conditions
oldPosition[1][1] += 0.03;
oldPosition[1][0] += 0.03;

// LJ variables
let epslon = 20;
let sigma = radius;




// Objects



// Functions

function draw(){

    context.fillStyle = '#30638E ';
    context.fillRect(0, 0, canvas.width, canvas.height);

    for (let i=0 ; i<position.length; i++){

        drawCircles(i)
    }
}

function drawCircles(i){

    
    context.beginPath();
    context.arc(position[i][0], position[i][1], radius/2, 0, 2*Math.PI);
    //context.fillStyle = '#000';
    context.fillStyle = 'hsl(0,100%,'+ distance(position) +'%)';    
    context.fill(); 
    context.closePath();
}

function lennardJones(i){
    return 4*epslon*((sigma/i)**12 - (sigma/i)**6)
}

function verlet(position){
    let x_Next = 0;
    let y_Next = 0;
    let aux = 0;
    for (var i=0; i<position.length; i++) {
        
        x_Next = 2*position[i][0]  - oldPosition[i][0] + aceleration[i][0] * dt * dt;
        oldPosition[i][0] = position[i][0]
        position[i][0] = x_Next

        y_Next = 2*position[i][1]  - oldPosition[i][1] + aceleration[i][1] * dt * dt;
        oldPosition[i][1] = position[i][1]
        position[i][1] = y_Next

        // Boundary Conditions
        if (position[i][0] < radius/2){
            aux = oldPosition[i][0] 
            oldPosition[i][0] = position[i][0]
            position[i][0] = aux
        } else if(position[i][0]>width-radius/2){
            aux = oldPosition[i][0] 
            oldPosition[i][0] = position[i][0]
            position[i][0] = aux
        }
        
        if (position[i][1] < radius/2){
            aux = oldPosition[i][1] 
            oldPosition[i][1] = position[i][1]
            position[i][1] = aux
        } else if (position[i][1] > height-radius/2){
            aux = oldPosition[i][1] 
            oldPosition[i][1] = position[i][1]
            position[i][1] = aux
        }
	}
    
    computeAceleration(position)
}

function computeAceleration(position){
    

    let dy = position[0][1] - position[1][1]
    let dx = position[0][0] - position[1][0]
    let r = Math.sqrt(dx*dx + dy*dy)
    let V = lennardJones(r);
    aceleration[0][0]  =  V * dx
    aceleration[1][0]  = -V * dx
    aceleration[0][1]  =  V * dy
    aceleration[1][1]  = -V * dy
}



function startPause(){
    running = !running;
    animate();
  
}

function reset(){
    initDist =  parseFloat(document.getElementById("initialD").value);
    oldPosition = [new THREE.Vector3(radius+initDist,0,0),new THREE.Vector3(-(radius+initDist),0,0)];
    position = [new THREE.Vector3(radius+initDist,0,0),new THREE.Vector3(-(radius+initDist),0,0)];
    velocity = [new THREE.Vector3(),new THREE.Vector3()];
    aceleration = [new THREE.Vector3(),new THREE.Vector3()];
    
}



function distance(position){
    let dy = position[0][1] - position[1][1]
    let dx = position[0][0] - position[1][0]
    return Math.sqrt(dx*dx + dy*dy)
}



function animate() {
    

    for (var step=0; step<stepsPerFrame; step++) {
        
        verlet(position)  
        
        
   }
    //console.log(distance(position))
    draw()
    
    if (running){
        requestAnimationFrame(animate)
    }
}



