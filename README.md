# LJ Box

Visit :  https://caiquesarkis.gitlab.io/lj-box

Author: Caique Sarkis ([@caique_sarkis](https://twitter.com/caique_sarkis))

Description: Simulação de dinâmica molecular que utiliza como potencial a interação do modelo de Lennard Jones

## References

[1] [Lennard Jones Potential](https://chem.libretexts.org/Bookshelves/Physical_and_Theoretical_Chemistry_Textbook_Maps/Supplemental_Modules_(Physical_and_Theoretical_Chemistry)/Physical_Properties_of_Matter/Atomic_and_Molecular_Properties/Intermolecular_Forces/Specific_Interactions/Lennard-Jones_Potential)   

[2] [Integração de Verlet](https://www.algorithm-archive.org/contents/verlet_integration/verlet_integration.html)
